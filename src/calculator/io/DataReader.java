package calculator.io;

import calculator.exceptions.NoSuchOperatorException;
import calculator.logic.Operator;

import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Scanner;

public class DataReader {
    Scanner sc = new Scanner(System.in);

    public void close(){ sc.close();}

    public void setLocale(){sc.useLocale(Locale.US);}

    public double getNumber() throws InputMismatchException {
        boolean inputOk = false;
        double a=0;
        while (!inputOk) {
            try {
                a = sc.nextDouble();
                inputOk = true;
            } catch (InputMismatchException e) {
                System.out.println("Wprowadź liczbe i pamiętaj by części dziesiętne oddzielić kropką");
                a=0;
            }
            sc.nextLine();
        }
        return a;
    }

    public Operator getOperator(){
        boolean inputOk = false;
        Operator operator = null;
        while (!inputOk) {
            try {
                operator = Operator.fromOperator(sc.nextLine().charAt(0));
                inputOk = true;
            } catch (NoSuchOperatorException e) {
                System.out.println(e.getMessage() + ". Wprowadź prawidłowe działane");
            }
        }
        return operator;
    }
}
