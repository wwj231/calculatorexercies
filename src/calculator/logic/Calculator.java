package calculator.logic;

import calculator.exceptions.DivisionByZeroException;

public class Calculator {

    public static void operation(double a, double b, Operator operator){
        switch (operator){
            case ADD:
                System.out.println(String.format("= %.2f", add(a, b)));
                break;
            case SUBTRACT:
                System.out.println(String.format("= %.2f", subtract(a,b)));
                break;
            case MULTIPLY:
                System.out.println(String.format("= %.2f", multiply(a,b)));
                break;
            case DIVIDE:
                try {
                    System.out.println(String.format("= %.2f", divide(a, b)));
                } catch (DivisionByZeroException e){
                    System.out.println(e.getMessage());
                }
                break;
            case POWER:
                System.out.println(String.format("=%.2f", power(a,b)));
                break;
        }
    }

    public static double add(double a, double b) {
        return a + b;
    }
    public static double subtract(double a, double b) {
        return a - b;
    }

    public static double multiply(double a, double b) {
        return a * b;
    }

    public static double divide(double a, double b) throws DivisionByZeroException {
        if (b==0){
            throw new DivisionByZeroException("Pamiętaj cholero, nie dziel przez zero");
        } else {
            return a / b;
        }
    }

    public static double power(double a, double b){
        return Math.pow(a,b);
    }
}
