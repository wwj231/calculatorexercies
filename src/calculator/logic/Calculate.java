package calculator.logic;

import calculator.io.DataReader;

public class Calculate {
    private DataReader dataReader = new DataReader();

    public void calculate(){
        dataReader.setLocale();
        System.out.println("Podaj a :");
        double a = dataReader.getNumber();
        System.out.println("Wybierz działanie : +, -, /, *, ^(potęgowanie)");
        Operator operator = dataReader.getOperator();
        System.out.println("Podaj b :");
        double b = dataReader.getNumber();
        Calculator.operation(a, b, operator);
        dataReader.close();
    }
}
