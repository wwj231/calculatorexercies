package calculator.logic;

import calculator.exceptions.NoSuchOperatorException;

public enum Operator {
    ADD('+'),
    SUBTRACT('-'),
    MULTIPLY('*'),
    DIVIDE('/'),
    POWER('^');

    private final char operator;

    Operator(char operator) {
        this.operator = operator;
    }

    public char getOperator() {
        return operator;
    }

    public static Operator fromOperator(char menuChoice) throws NoSuchOperatorException {
        Operator[] values = values();
        for (Operator op : values) {
            if (op.operator == menuChoice)
                return op;
        }
        throw new NoSuchOperatorException("Nie ma takiej opcji : " + menuChoice);
    }
}
