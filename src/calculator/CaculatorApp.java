package calculator;

import calculator.logic.Calculate;

public class CaculatorApp {
    static final String version = "calculator exercise version 0.1";

    public static void main(String[] args) {
        System.out.println(version);
        Calculate calculate = new Calculate();
        calculate.calculate();
    }
}
