package calculator.exceptions;

public class NoSuchOperatorException extends Exception {
    public NoSuchOperatorException(String message) { super(message);}
}
